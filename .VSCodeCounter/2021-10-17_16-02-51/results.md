# Summary

Date : 2021-10-17 16:02:51

Directory c:\sourcecode\repos\mark\reckon

Total : 10 files,  194 codes, 14 comments, 56 blanks, all 264 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C# | 9 | 179 | 14 | 52 | 245 |
| XML | 1 | 15 | 0 | 4 | 19 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 10 | 194 | 14 | 56 | 264 |
| reckontest | 10 | 194 | 14 | 56 | 264 |
| reckontest\Classes | 1 | 39 | 2 | 13 | 54 |
| reckontest\Controllers | 1 | 26 | 4 | 11 | 41 |
| reckontest\Models | 5 | 42 | 6 | 15 | 63 |

[details](details.md)