# Details

Date : 2021-10-17 16:02:51

Directory c:\sourcecode\repos\mark\reckon

Total : 10 files,  194 codes, 14 comments, 56 blanks, all 264 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [reckontest/Classes/getJson.cs](/reckontest/Classes/getJson.cs) | C# | 39 | 2 | 13 | 54 |
| [reckontest/Controllers/Test1Controller.cs](/reckontest/Controllers/Test1Controller.cs) | C# | 26 | 4 | 11 | 41 |
| [reckontest/Models/Divsor.cs](/reckontest/Models/Divsor.cs) | C# | 8 | 1 | 3 | 12 |
| [reckontest/Models/OutPut.cs](/reckontest/Models/OutPut.cs) | C# | 8 | 1 | 2 | 11 |
| [reckontest/Models/OutputDetails.cs](/reckontest/Models/OutputDetails.cs) | C# | 11 | 2 | 5 | 18 |
| [reckontest/Models/Range.cs](/reckontest/Models/Range.cs) | C# | 8 | 1 | 3 | 12 |
| [reckontest/Models/ResultResponse.cs](/reckontest/Models/ResultResponse.cs) | C# | 7 | 1 | 2 | 10 |
| [reckontest/Program.cs](/reckontest/Program.cs) | C# | 24 | 0 | 3 | 27 |
| [reckontest/Startup.cs](/reckontest/Startup.cs) | C# | 48 | 2 | 10 | 60 |
| [reckontest/reckontest.csproj](/reckontest/reckontest.csproj) | XML | 15 | 0 | 4 | 19 |

[summary](results.md)