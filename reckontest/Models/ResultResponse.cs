//using System;

namespace reckontest.Models
{
    public class ResultResponse
    {
        public string Result { get; set; }
        public Range Range { get; set; }
        public DivisorDetails DivisorDetails { get; set; }

        public TextToSearch TextToSearch { get; set; }

        public SubTexts SubTexts { get; set; }

    }
}
