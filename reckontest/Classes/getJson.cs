using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Threading;
using System;
using Newtonsoft.Json;


namespace reckontest.Classes
{
    class getJson
    {
        public getJson()
        {

        }

        public async Task<string> tryIt(Uri url)

        {

            Console.WriteLine($"getJson for {url.ToString()}  ");

            HttpClient http = new HttpClient((new RetryHttp(new HttpClientHandler())));
            var response = await http.GetAsync(url);

            // Deserialize it and Serialize it again to make sure its valid Json
            // Throw an Error if not

            if (response.StatusCode == HttpStatusCode.OK)
            {
                try
                {
                    var jsonString = response.Content.ReadAsStringAsync().Result;
                    var json = JsonConvert.DeserializeObject(jsonString);
                    return JsonConvert.SerializeObject(json);
                }
                catch (Exception error)
                {
                    // Something wrong with the JOSN
                    throw new Exception($"Invalid JSON Received from API {error.Message}");
                }
            }
            else
            {
                throw new Exception($"Unable to get a response to getJson from given url. Reason [{response.ReasonPhrase}]");
            }

        }

    }

    public class RetryHttp : DelegatingHandler
    {
        // Ugh, Ok .. but .. I have Limited Retries, so it does not go on forever

        // I know the requirement said " if it's the case that one of the endpoints fail, have the code try again until successful results  are returned. "
        // however, if th server is down, thsi will cause the app to become unresponsine and it will HttpMessageHandler

        // The best approach would be to add logic to determine if DNS was working (ie: valid url),
        // server was alive but the api was just not responding .. if it was seriously a requirement to do this.

        // Generally APis work or they done, if it was that unreliable, one would likely find the api developer and shoot them 
        // for allowing that sort of code into prod.

        // After retries exceed, return whatever response we get for caller to deal with

        private const int MaxRetries = 3;

        public RetryHttp(HttpMessageHandler innerHandler) : base(innerHandler) { }

        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpResponseMessage response = null;
            for (int i = 0; i < MaxRetries; i++)

            {
                response = await base.SendAsync(request, cancellationToken);

                Console.WriteLine($"Try {i.ToString()} to {request.RequestUri.ToString()}  ");

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return response;
                }
            }

            Console.WriteLine($"Failed to {request.RequestUri.ToString()}  ");

            return response;
        }
    }


}

