﻿using System;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using reckontest.Classes;
using Newtonsoft.Json;


namespace reckontest.Controllers
{
    [Produces(MediaTypeNames.Application.Json)]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly ILogger<TestController> _logger;

        public TestController(ILogger<TestController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Models.ResultResponse>> test1()
        {

            try
            {

                var client = new getJson();

                // Hit Remote API for Range and Divisor Info
                // returns a guaranteed valid serialized JSOn String or throws an Error
                // will retry 3 times as the API is supposedly unreliable
                // Cast to a Class matching the data during Deserialization
                var rangeInfo = JsonConvert.DeserializeObject<Models.Range>(await client.tryIt(new Uri("https://join.reckon.com/test1/rangeInfo")));
                var divisorInfo = JsonConvert.DeserializeObject<Models.DivisorDetails>(await client.tryIt(new Uri("https://join.reckon.com/test1/divisorInfo")));

                for (int i = rangeInfo.lower; i <= rangeInfo.upper; i++)
                {
                    var result = "";

                    for (int j = 0; j <= divisorInfo.outputDetails.Length - 1; j++)
                    {
                        var k = divisorInfo.outputDetails[j];

                        // Zero divided by any number is always 0, technically, its not divisible, but Modulus will return 0 
                        // so ensure we do not include a zero value for i
                        if (i % k.divisor == 0 && i > 0)
                        {

                            result = $"{result} {k.output}";

                        }

                    }


                    Console.WriteLine($"{i.ToString()}: {result}  ");

                }

                return new Models.ResultResponse { Result = "Success - Please check the console log for the Result", Range = rangeInfo, DivisorDetails = divisorInfo };

            }
            catch (Exception error)
            {

                return new Models.ResultResponse { Result = $"Test1: There was an issue with the Range or Divisor API Lookups {error}" };
            }

        }

        [HttpGet]
        [Route("/test2")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Models.ResultResponse>> test2()
        {

            try
            {

                var client = new getJson();

                // Hit Remote API for Range and Divisor Info
                // returns a guaranteed valid serialized JSOn String or throws an Error
                // will retry 3 times if APi is unreliable
                // Cast to a Range Object
                var textToSearch = JsonConvert.DeserializeObject<Models.TextToSearch>(await client.tryIt(new Uri("https://join.reckon.com/test2/textToSearch")));
                var subTexts = JsonConvert.DeserializeObject<Models.SubTexts>(await client.tryIt(new Uri("https://join.reckon.com/test2/subTexts")));


                //The solution should match the subtexts against the text , outputting the positions of the beginning of each match for the subtext within  the textToSearch. 
                //The set of characters can occur anywhere within the string. 
                //Multiple matches are possible 
                //Matching is case insensitive 
                //If no matches have been found, “<No Output>” is generated 
                //Your api endpoint should POST the results to the endpoint at https://join.reckon.com/test2/submitResults with the results in the format  listed below.  
                //The api endpoints provided can be quite unreliable, if it's the case that one of the endpoints fail, try them again until succesful results are  returned. 

                //=============================================================================
                // Watch this Space 

                return new Models.ResultResponse { Result = "Success", TextToSearch = textToSearch, SubTexts = subTexts };

            }
            catch (Exception error)
            {

                return new Models.ResultResponse { Result = $"Test2: There was an issue with the Range or Divisor API Lookups {error}" };
            }

        }
    }
}
